import prisma from '@/lib/prisma';

const handler = async (req, res) => {
    if (req.method === 'GET') await getHistory(req, res);
    else if (req.method === 'POST') await addHistory(req, res);
    else methodNotAllowed(req, res);
}

const getHistory = async (req, res) => {
    const {userId, limit, page} = req.query;
    const take = Math.min(5, Number(limit ?? 5));
    const skip = take * Math.max(0, Number(page ?? 1) - 1);
    try {
        const filter = userId ? {equals: String(userId)} : {gt: ''}
        const history = await prisma.history.findMany({
            where: {
                userId: filter
            },
            orderBy: [
                {createdAt: 'asc'},
            ],
            skip: skip,
            take: take,
        });
        res.send({
            list: history,
            length: await prisma.history.count({
                where: {
                    userId: filter
                }
            })
        });
    } catch {
        badRequest(req, res);
    }
}

const addHistory = async (req, res) => {
    const {userId, action, post} = JSON.parse(req.body);
    try {
        const result = await prisma.history.create({
            data: {
                userId: userId,
                action: action,
                post: post,
            },
        });
        res.send(result);
    } catch {
        badRequest(req, res);
    }
}

const badRequest = (req, res) => res.status(400).send('Bad request');
const methodNotAllowed = (req, res) => res.status(405).send('Method Not Allowed');

export default handler;
